﻿open Suave
open Suave.Http
open Suave.Http.Applicatives
open Suave.Http.Successful
open Suave.Http.RequestErrors
open Suave.Http.Writers
open Suave.Web
open Suave.Log
open Suave.Types
open WorldCup2014.LiveService.FSharp
open Newtonsoft.Json

do Manager.configure ({ Manager.default_config with UseTimeZoneService = true })

let json obj =
    set_mime_type "application/json; charset=utf-8"
    >> OK (JsonConvert.SerializeObject obj)

let run f = f() |> Async.RunSynchronously

let getCached f =
    let r = lazy (run f)
    warbler (fun _ -> json (r.Force()))

let get f =
    warbler (fun _ -> json (run f))
    
let logger =
    Loggers.CombiningLogger
        [ Loggers.ConsoleWindowLogger(LogLevel.Debug, colourise = true) ]

let allow_cross_site_origin = set_header "Access-Control-Allow-Origin" "*"

let app =
    choose [
        log logger log_format >>= never
        GET >>= choose [
            allow_cross_site_origin >>
            choose [
                url "/teams" >>= getCached Manager.asyncGetTeams
                url "/coaches" >>= getCached Manager.asyncGetCoaches
                url "/players" >>= getCached Manager.asyncGetPlayers
                url "/matches" >>= get Manager.asyncGetMatches
                url "/lastMatch" >>= get Manager.asyncGetLastMatch ]
        ]
        NOT_FOUND "Found no handlers."
    ]

let config =
    { default_config with logger = logger }

do web_server config app
