﻿namespace WorldCup2014.LiveService.FSharp

open System
open System.ComponentModel
open WorldCup2014.LiveService
open WorldCup2014.LiveService.Parser
open XTract

module Manager =

    let default_config = Parser.default_config

    let configure c = Parser.configure c

    let asyncGetPlayers () = 
        asyncParse "http://de.fifa.com/worldcup/players/browser/index.html"
                   (select """//script[@id="playerJSon"]""" >> Node.innerText)
                   parsePlayers

    let asyncGetTeams () =
        asyncParse "http://de.fifa.com/worldcup/teams/team=43971/profile.html"
                   (select """//script[@type="horizontal/navigation"]""" >> Node.innerText)
                   parseTeams

    let asyncGetMatches () =
        asyncParse "http://de.fifa.com/worldcup/matches/index.html"
                   (selectList """//*[@data-id]""")
                   parseMatches

    let asyncGetCoaches () =
        asyncParse "http://www.fifa.com/worldcup/players/coaches/index.html"
                   (selectList """//div[@data-player-id]""")
                   parseCoaches

    let asyncGetLastMatch () = async {
        let now = DateTimeOffset.Now
        let! matches = asyncGetMatches()
        return
            query { for m in matches do
                    sortByDescending (m.DateTime)
                    where (m.DateTime < now)
                    head }
    }

namespace WorldCup2014.LiveService

module Manager =

    open WorldCup2014.LiveService.FSharp.Manager

    let Configure (c: System.Action<Configuration>) =
        let mutable config = default_config
        c.Invoke(config)
        Parser.configure(config)

    let GetPlayersAsync()   = asyncGetPlayers()   |> Async.StartAsTask
    let GetTeamsAsync()     = asyncGetTeams()     |> Async.StartAsTask
    let GetMatchesAsync()   = asyncGetMatches()   |> Async.StartAsTask
    let GetCoachesAsync()   = asyncGetCoaches()   |> Async.StartAsTask
    let GetLastMatchAsync() = asyncGetLastMatch() |> Async.StartAsTask
