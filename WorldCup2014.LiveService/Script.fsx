﻿#r "System.Xml.Linq"
#r "bin/Debug/HttpClient.dll"
#r "bin/Debug/HtmlAgilityPack.dll"
#r "bin/Debug/XTract.dll"
#r @"..\packages\FSharp.Data.2.0.9\lib\net40\FSharp.Data.dll"

#load "Types.fs"
#load "Utils.fs"
#load "TimeZone.fs"
#load "Parser.fs"
#load "Manager.fs"

open WorldCup2014.LiveService

let run tsk = tsk |> Async.RunSynchronously

let teams = run <| Manager.asyncGetTeams()
let players = run <| Manager.asyncGetPlayers()
let coaches = run <| Manager.asyncGetCoaches()
let matches = run <| Manager.asyncGetMatches()

let tzoffsetManaus = TimeZone.tryFindTimeZoneOffset "Manaus, Bra"
let tzoffsetNatal = TimeZone.tryFindTimeZoneOffset "Natal, Bra"
