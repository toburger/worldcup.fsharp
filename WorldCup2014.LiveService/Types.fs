﻿namespace WorldCup2014.LiveService

open System

type Player =
    { Name: string
      Team: string
      Picture: string
      BirthDate: DateTime }

type Team =
    { Name: string
      CountryCode: string
      SmallImage: string
      MediumImage: string
      BigImage: string }

type Score =
    | Score of int * int
    | NoScore
    
type MatchStatus =
    | Bevorstehend
    | ImGange of TimeSpan
    | Ausgetragen
    override self.ToString() =
        match self with
        | Bevorstehend -> "Bevorstehend"
        | ImGange t -> "Im Gange (0:0)"
        | Ausgetragen -> "Ausgetragen"

type Match =
    { Team1: string
      Team2: string
      Score: Score
      DateTime: DateTimeOffset
      MatchNr: string
      Group: string
      Stadium: string
      Location: string
      Status: MatchStatus }
    override self.ToString() =
            match self.Score with
            | Score (s1, s2) -> sprintf "%s (%d) vs %s (%d)" self.Team1 s1 self.Team2 s2
            | NoScore -> sprintf "%s vs %s" self.Team1 self.Team2

type Coach =
    { Name: string
      Picture: string }

[<CLIMutable>]
type Configuration = { UseTimeZoneService: bool }
