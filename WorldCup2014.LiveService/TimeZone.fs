﻿module TimeZone

open System
open HttpClient
open FSharp.Data

type DaylightSaving =
    | DaylightSaving
    | NoDayligtSaving
    | UnknownDST

let private (>>=) option binder = Option.bind binder option
type private GoogleGeoCode = JsonProvider<"""{"results":[{"address_components":[{"long_name":"Natal","short_name":"Natal","types":["locality","political"]},{"long_name":"Natal","short_name":"Natal","types":["administrative_area_level_2","political"]},{"long_name":"RioGrandedoNorte","short_name":"RN","types":["administrative_area_level_1","political"]},{"long_name":"Brasilien","short_name":"BR","types":["country","political"]}],"formatted_address":"Natal-RioGrandedoNorte,Brasilien","geometry":{"bounds":{"northeast":{"lat":-5.7007113,"lng":-35.1533204},"southwest":{"lat":-5.8991266,"lng":-35.2914161}},"location":{"lat":-5.7792569,"lng":-35.200916},"location_type":"APPROXIMATE","viewport":{"northeast":{"lat":-5.7007113,"lng":-35.1533204},"southwest":{"lat":-5.8991266,"lng":-35.2914161}}},"types":["locality","political"]}],"status":"OK"}""">
type private TimeZoneOffset = XmlProvider<"""<?xml version="1.0" encoding="ISO-8859-1" ?><timezone xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="http://www.earthtools.org/timezone.xsd"><version>1.0</version><location><latitude>-5.7792569</latitude><longitude>-35.200916</longitude></location><offset>-3</offset><suffix>P</suffix><localtime>19 Jun 2014 04:08:38</localtime><isotime>2014-06-19 04:08:38 -0300</isotime><utctime>2014-06-19 07:08:38</utctime><dst>Unknown</dst></timezone>""">

let brazilianTimeZone =
    try TimeZoneInfo.FindSystemTimeZoneById "E. South America Standard Time"
    with _ -> TimeZoneInfo.FindSystemTimeZoneById "Brazil/East" // Mono

let tryGetCoordinates address =
    let url = sprintf "http://maps.googleapis.com/maps/api/geocode/json?address=%s" address
    let request = createRequest Get url |> withResponseCharacterEncoding "utf-8"
    let response = request |> getResponse
    match response.StatusCode, response.EntityBody with
    | 200, Some body ->
        let geocode = GoogleGeoCode.Parse(body)
        if geocode = null || geocode.Status <> "OK" then None
        else
            let result = geocode.Results.[0]
            let location = result.Geometry.Location
            let formattedAddress = result.FormattedAddress
            Some (formattedAddress, (location.Lat, location.Lng))
    | _ -> None

let private parseDaylightSaving s =
    match s with
    | "True" -> DaylightSaving
    | "False" -> NoDayligtSaving
    | _ -> UnknownDST

let tryGetTimeZoneOffset (lat, lon) =
    let url = sprintf "http://www.earthtools.org/timezone/%M/%M" lat lon
    let request = createRequest Get url |> withResponseCharacterEncoding "utf-8"
    let response = request |> getResponse
    match response.StatusCode, response.EntityBody with
    | 200, Some body ->
        let timezone = TimeZoneOffset.Parse(body)
        match timezone with
        | null -> None
        | timezone -> Some (timezone.Offset, parseDaylightSaving timezone.Dst)
    | _ -> None

let tryFindTimeZoneOffset address =
    tryGetCoordinates address >>= (snd >> tryGetTimeZoneOffset)

let tryFindTimeZoneOffsetMemoized =
    Utils.memoize tryFindTimeZoneOffset
