﻿module internal WorldCup2014.LiveService.Parser

open System
open System.Globalization
open HttpClient
open XTract
open FSharp.Data
open Utils
open WorldCup2014.LiveService

let default_config = { UseTimeZoneService = true }

let mutable private config = default_config

let configure config' = config <- config'

type Players = JsonProvider<"""[{"idcupseason":251164,"idteam":43843,"teamname":"Algerien","countrycode":"ALG","idplayer":356166,"bibnum":1,"fieldpos":1,"webname":"Cedric SI MOHAMMED","webnamealt":"","surname":"SI MOHAMMED","shirtname":"SI MOHAMMED","playerletter":"S","birthdate":"1985-01-09 00:00:00","clubname":"CS Constantine","idclub":2147483622}]""">
type Teams   = JsonProvider<"""[{"type":"teams","title":"Mannschaften","type_link":"/worldcup/teams/index.html","idteam":"43843","webname":"Algerien","countrycode":"ALG","idgroup":"255947","groupname":"Group H","groupnameabbr":"H","link":"/worldcup/teams/team=43843/index.html"}]""">

let asyncLoad url = async {
    let request = createRequest Get url
                  |> withResponseCharacterEncoding "utf-8"
    let! response = request |> getResponseBodyAsync
    return Html.load response
}

let asyncParse url selector parser = async {
    let! doc = asyncLoad url
    return
        match selector doc with
        | Some x -> parser x
        | None -> [||]
}

let parsePlayer (p: Players.Root) =
    { Name = p.Webname
      BirthDate = p.Birthdate
      Team = p.Teamname
      Picture = sprintf "http://img.fifa.com/images/fwc/2014/players/prt-2/%i.png" p.Idplayer }

let parsePlayers =
    Players.Parse >> Array.map parsePlayer

let parseTeam (t: Teams.Root) =
    let img i = sprintf "http://img.fifa.com/images/flags/%i/%s.png" i <| t.Countrycode.ToLower()
    { Name = t.Webname
      CountryCode = t.Countrycode
      SmallImage = img 3
      MediumImage = img 4
      BigImage = img 5 }

let parseTeams =
    Teams.Parse >> Array.map parseTeam

let extractScore (s: string) =
    if s.Contains "-" then
        match s.Split('-') with
        | [| s1; s2 |] -> Some(int s1, int s2)
        | _ -> None
    else None

let extractDateTime (s: string) =
    match DateTime.TryParseExact(s,
                                 [| @"dd MMM yyyy - HH:mm Or\t\s\zei\t"
                                    @"dd MMM yyyy - HH:mm" |],
                                 CultureInfo("de-DE"),
                                 DateTimeStyles.AllowLeadingWhite ||| DateTimeStyles.AllowTrailingWhite) with
    | true, d -> Some d
    | false, _ -> None

let adjustToBrazilianTime (date: DateTime option) = maybe {
    let! d = date
    let tz = TimeZone.brazilianTimeZone
    return DateTimeOffset(d, tz.GetUtcOffset(d))
}

let adjustToTimeInLocation address (date: DateTime option) = maybe {
    if config.UseTimeZoneService then
        let! d = date
        let! a = address
        let! tzOffset = TimeZone.tryFindTimeZoneOffsetMemoized a
        return DateTimeOffset(d, TimeSpan.FromHours(float <| fst tzOffset))
    else ()
}

let extractTime (s: string) =
    match TimeSpan.TryParseExact(s, "hh\:mm", CultureInfo("de-DE")) with
    | true, t -> Some t
    | false, _ -> None

let parseMatch html =
    let doc = Html.load html

    let get who =
        let flag =
            maybe { let! node = Node.select doc (sprintf """//div[@class="t %s"]/div[1]/span[1]/img""" who)
                    return node.Attributes.["src"].Value }
        let name = Node.select doc (sprintf """//div[@class="t %s"]/div[2]/span[1]""" who) |> Node.innerText
        let countryName = Node.select doc (sprintf """//div[@class="t %s"]/div[2]/span[2]""" who) |> Node.innerText
        flag, name, countryName

    let home = get "home"
    let away = get "away"

    let score =
        maybe { let! text = Node.select doc """//span[@class="s-scoreText"]""" |> Node.innerText
                return! extractScore text }
    let matchnr = Node.select doc """//div[@class="mu-i-matchnum"]""" |> Node.innerText
    let group = Node.select doc """//div[@class="mu-i-group"]""" |> Node.innerText
    let stadium = Node.select doc """//div[@class="mu-i-stadium"]""" |> Node.innerText
    let location = Node.select doc """//div[@class="mu-i-venue"]""" |> Node.innerText
    let status =
        maybe { let! text = Node.select doc """//div[@class="s-status-abbr"]""" |> Node.innerText
                return
                    match text with
                    | "Ende" -> Ausgetragen
                    | "?" -> ImGange(TimeSpan.Zero) // TODO: parsen
                    | _ -> Bevorstehend }
    let datetimelocal =
        maybe { let! text = Node.select doc """//div[@class="mu-i-datetime"]""" |> Node.innerText 
                return! text |> String.stripTags |> extractDateTime }
    let datetimeutc =
        maybe { let! node = Node.select doc """//*[@data-timeutc]"""
                return! node.Attributes.["data-timeutc"].Value |> extractTime }
    let datetime =
        maybe { let! datetimelocal = datetimelocal
                let! datetimeutc = datetimeutc
                let offset = datetimeutc - datetimelocal.TimeOfDay
                return DateTimeOffset(datetimelocal, -offset) }
    let bestGuessDateTime = datetime <*> adjustToTimeInLocation location datetimelocal <*> adjustToBrazilianTime datetimelocal
    let orEmpty opt = opt |> defaultArg <| ""
    { Team1 = home |> fun (_, n, _) -> n |> orEmpty
      Team2 = away |> fun (_, n, _) -> n |> orEmpty
      Score = match score with Some s -> Score s | None -> NoScore
      DateTime = bestGuessDateTime |> defaultArg <| DateTimeOffset.MinValue
      MatchNr = matchnr |> orEmpty
      Group = group |> orEmpty
      Stadium = stadium |> orEmpty
      Location = location |> orEmpty
      Status = status |> defaultArg <| Bevorstehend }

let parseMatches =
    List.toArray
    >> Array.Parallel.map (fun (node: HtmlAgilityPack.HtmlNode) ->
        parseMatch node.InnerHtml)
    >> Array.sortBy (fun {DateTime = dateTime} -> dateTime)
    >> Seq.distinct
    >> Seq.toArray

let parseCoach html =
    let doc = Html.load html
    let name = Node.select doc """//span[@class="p-n-webname"]""" |> Node.innerText
    let picture =
        maybe { let! node = Node.select doc """//img[@class="player"]"""
                return node.Attributes.["src"].Value }
    { Name = name |> defaultArg <| ""
      Picture = picture |> defaultArg <| "" }

let parseCoaches =
    List.toArray
    >> Array.Parallel.map (fun (node: HtmlAgilityPack.HtmlNode) ->
        parseCoach node.InnerHtml)
    >> Array.filter (fun { Name = name } -> name <> "")

let select xpath doc =
    Node.select doc xpath

let selectList xpath doc =
    Node.selectList doc xpath
