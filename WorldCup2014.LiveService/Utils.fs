﻿module Utils

type MaybeBuilder() =
    member self.Bind(m, f) = m |> Option.bind f
    member self.Return v = Some v
    member self.ReturnFrom v = v
    member self.Zero() = None

let maybe = MaybeBuilder()

[<AutoOpen>]
module Option =
    let orElse o1 o2 =
        match o1 with
        | Some _ -> o1
        | None -> o2

    let (<*>) o1 o2 = orElse o1 o2
        
/// http://blogs.msdn.com/b/dsyme/archive/2007/05/31/a-sample-of-the-memoization-pattern-in-f.aspx
let memoize f =
    let cache = ref Map.empty
    fun x ->
        match (!cache).TryFind(x) with
        | Some res -> res
        | None ->
            let res = f x
            cache := (!cache).Add(x, res)
            res
