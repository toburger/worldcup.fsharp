# README #

A sample to show how to integrate C# with F# and also a nice demonstration of the possibilities you have by using F#.

### What is this repository for? ###

* To query FIFA WorldCup 2014 teams, players and upcoming matches/match results
* To demonstrate Rudi the power of F#! ;)

### How do I get set up? ###

* Clone it
* Open the Solution with Visual Studio
* Make sure the startup project is set to the console app
* Compile it
* Run it

### Who do I talk to? ###

* Rudi
* Other people interested in learning F#