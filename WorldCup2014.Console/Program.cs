﻿using System;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using WorldCup2014.LiveService;

namespace FSCompiler
{
    class Program
    {
        static void Main(string[] args)
        {
            Manager.Configure(config => config.UseTimeZoneService = false);

            Task.Run(async () =>
            {
                var info = "Enter t (Teams), p (Players), c (Coaches), m (Matches), lm (Last Match) or q (Quit): ";
                Console.Write(info);
                string s;
                while ((s = Console.ReadLine()) != "q")
                {
                    switch (s)
                    {
                        case "t":
                            var teams = await Manager.GetTeamsAsync();
                            foreach (var team in teams)
                            {
                                Console.WriteLine("{0} ({1})", team.Name, team.CountryCode);
                            }
                            break;
                        case "p":
                            var players = await Manager.GetPlayersAsync();
                            foreach (var player in players.OrderBy(player => player.BirthDate))
                            {
                                Console.WriteLine("{0} ({1:d}), {2}", player.Name, player.BirthDate, player.Team);
                            }
                            break;
                        case "c":
                            var coaches = await Manager.GetCoachesAsync();
                            foreach (var coach in coaches)
                            {
                                Console.WriteLine("{0}", coach.Name);
                            }
                            break;
                        case "m":
                            var matches = await Manager.GetMatchesAsync();
                            foreach (var match in matches.OrderBy(match => match.DateTime))
                            {
                                PrintMatch(match);
                            }
                            break;
                        case "lm":
                            var lmatch = await Manager.GetLastMatchAsync();
                            PrintMatch(lmatch);
                            break;
                    }
                    Console.Write(info);
                }
            }).Wait();
        }

        private static void PrintMatch(Match match)
        {
            var localDateTime = match.DateTime.LocalDateTime;
            Console.WriteLine("{0:G} ({1:T}) - {2}: {3} [{4}]", match.DateTime, localDateTime, match.Group, match.ToString(), match.Status);
        }
    }
}
